package net.IslandEarth.PvP.profile;

import lombok.Getter;
import lombok.Setter;

public class Level {

	@Getter @Setter private int level;
	@Getter @Setter private int xp;

    public Level(int level, int xp) {
        this.level = level;
        this.xp = xp;
    }
}
