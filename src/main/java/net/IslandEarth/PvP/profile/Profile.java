package net.IslandEarth.PvP.profile;

import java.util.UUID;

import lombok.Getter;

public class Profile {

	@Getter private UUID uniqueId;
    @Getter private int kills;
    @Getter private int deaths;
    @Getter private int kdr;
    @Getter private Level level;

    public Profile(UUID uniqueId, int kills, int deaths, Level level) {
    	this.uniqueId = uniqueId;
		this.kills = kills;
		this.deaths = deaths;
		this.level = level;
		this.kdr = kills / deaths;
    }

}
