package net.IslandEarth.PvP.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.IslandEarth.PvP.IslandEarth;

public class KitsManager {
	
	private IslandEarth plugin;
	
	public KitsManager(IslandEarth plugin) 
	{
		this.plugin = plugin;
	}
	
	public void addKit(Player creator, String name) 
	{
		File file = new File(plugin.getDataFolder() + "/kits/" + name + ".yml");
		
		if(file.exists()) 
		{
			creator.sendMessage(ChatColor.RED + "That kit already exists!");
			return;
		}
		
		try {
			
			if(!file.createNewFile()) creator.sendMessage(ChatColor.RED + "An error occured while creating kitdata file.");
			creator.sendMessage(ChatColor.GREEN + "Created kit!");

			FileConfiguration kit = YamlConfiguration.loadConfiguration(file);
			kit.set("inventory", creator.getInventory().getContents());
			
			try {
				kit.save(file);
				kit.load(file);
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void giveKit(Player player, String name) 
	{
		File file = new File(plugin.getDataFolder() + "/kits/" + name + ".yml");
		
		if(!file.exists()) 
		{
			player.sendMessage(ChatColor.RED + "That kit does not exist!");
			return;
		}
		
		FileConfiguration kit = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + "/kits/" + name + ".yml"));
		List<ItemStack> items = (List<ItemStack>) kit.getList("inventory");
		
		player.getInventory().setContents(items.toArray(new ItemStack[0]));
		player.sendMessage(ChatColor.AQUA + "You chose the kit " + ChatColor.WHITE + name + ChatColor.AQUA + "!");

		player.sendTitle("", ChatColor.AQUA + name, 20, 60, 20);
	}
	
	public void removeKit(Player remover, String name) 
	{
		File file = new File(plugin.getDataFolder() + "/kits/" + name + ".yml");
		if(!file.exists()) 
		{
			remover.sendMessage(ChatColor.RED + "That kit does not exist!");
		} else if (!file.delete()) remover.sendMessage(ChatColor.RED + "An error occured while removing the kitdata file.");
	}
}
