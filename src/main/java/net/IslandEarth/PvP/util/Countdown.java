package net.IslandEarth.PvP.util;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import net.IslandEarth.PvP.IslandEarth;

public abstract class Countdown {
	
    private int time;

    protected BukkitTask task;
    protected final IslandEarth plugin;

    public Countdown(int time, IslandEarth plugin) {
    	this.time = time;
    	this.plugin = plugin;
    }

    public abstract void count(int current);

    public final void start() {
    	task = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
    		count(time);
            if(time-- <= 0) cancel();
    	}, 0, 20);
    }
    
    private void cancel() {
    	Bukkit.getScheduler().cancelTask(task.getTaskId());
    }
}
