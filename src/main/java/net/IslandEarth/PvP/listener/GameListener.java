package net.IslandEarth.PvP.listener;

import net.IslandEarth.PvP.util.WrapperPlayServerCamera;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.IslandEarth.PvP.IslandEarth;
import net.IslandEarth.PvP.util.Countdown;

import java.text.DecimalFormat;

public class GameListener implements Listener {

    private IslandEarth plugin;
    
    public GameListener(IslandEarth plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onDeath(PlayerDeathEvent pde) {
    	
    	Player victim = pde.getEntity();
    	if(pde.getEntity().getKiller() != null) {
    		
        	Player killer = pde.getEntity().getKiller();
        	
        	killer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, 1));
        	killer.playSound(killer.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
        	
        	respawn(victim);

            pde.setDeathMessage(null);
            pde.getDrops().clear();
            pde.setKeepLevel(true);
            pde.setDroppedExp(0);

        	killer.sendMessage(ChatColor.GREEN + "You killed " + victim.getName() + ".");
        	victim.sendMessage(ChatColor.RED + "You died to " + killer.getName() + " (" + new DecimalFormat().format(killer.getHealth()) + " HP)");
        	
        	Bukkit.getScheduler().runTaskLater(plugin, () -> {
        		victim.teleport(killer);
            	victim.setGameMode(GameMode.SPECTATOR);
            	victim.setSpectatorTarget(killer);
            	
            	WrapperPlayServerCamera packet = new WrapperPlayServerCamera();
            	packet.setCameraId(killer.getEntityId());
            	packet.sendPacket(victim);
        	}, 20);
    	} else respawn(victim);
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent pie) {
    	Player player = pie.getPlayer();
    	Action action = pie.getAction();
    	
    	if(action == Action.RIGHT_CLICK_AIR || action == Action.LEFT_CLICK_AIR) {
    		if(player.getInventory().getItemInMainHand().getType() == Material.MUSHROOM_STEW && player.getHealth() < 20) {
    			player.setHealth(player.getHealth() + 7 > player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() ? player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() : player.getHealth() + 7);
				player.getInventory().getItemInMainHand().setType(Material.BOWL);
				player.getWorld().playSound(player.getLocation(), Sound.ENTITY_PLAYER_BURP, 3, 1);
				player.setFoodLevel(20);
    		}
    	}
    }
    
    private void respawn(Player victim) {

        victim.spigot().respawn();

    	new Countdown(10, plugin) {

			@SuppressWarnings("deprecation")
			@Override
    		public void count(int count) {
                victim.sendTitle(ChatColor.RED + "You died!", ChatColor.GRAY + "Respawning in " +  count + "seconds.", 5, 20, 5);

    			if(count <= 0) {
    				victim.setGameMode(GameMode.ADVENTURE);
    				victim.resetTitle();
					victim.sendTitle(ChatColor.GREEN + "Respawed!", "" +  count + "seconds.", 5, 20, 20);
    				Location location = new Location(victim.getWorld(), -471, 70, -877, 90, 0);
    				victim.teleport(location);
    				victim.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
    			}
    		}
    	                                   
    	}.start();
    }
}
