package net.IslandEarth.PvP.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerPingListener implements Listener {

    @EventHandler
    public void onPing(ServerListPingEvent e) {
        e.setMotd("                   §6§lIslandEarth PvP" + "\n"
                + "         §7Custom PvP experience §e1.8-1.12");

        e.setMaxPlayers(35);
    }
}
