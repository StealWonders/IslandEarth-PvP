package net.IslandEarth.PvP.listener;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class GlobalListener implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent e) { 
    	e.setJoinMessage(ChatColor.GREEN + "" + e.getPlayer().getName() + " joined.");
    	if(!e.getPlayer().isOp()) e.getPlayer().setGameMode(GameMode.ADVENTURE);
    }

    @EventHandler
    public void playerQuit(PlayerQuitEvent e) { 
    	e.setQuitMessage(ChatColor.RED + "" + e.getPlayer().getName() + " left.");
    }
    
    @EventHandler
    public void onChat(AsyncPlayerChatEvent apce) {
    	Player player = apce.getPlayer();
    	apce.setFormat(ChatColor.WHITE + player.getName() + ": " + apce.getMessage());
    }
    
    @EventHandler
    public void onPlace(BlockPlaceEvent bpe) {
    	Player player = bpe.getPlayer();
    	if(!player.isOp() || !player.hasPermission("IslandEarth.build")) bpe.setCancelled(true);
    }
    
    @EventHandler
    public void onBreak(BlockBreakEvent bbe) {
    	Player player = bbe.getPlayer();
    	if(!player.isOp() || !player.hasPermission("IslandEarth.build")) bbe.setCancelled(true);
    }
}
