package net.IslandEarth.PvP.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.IslandEarth.PvP.IslandEarth;
import net.IslandEarth.PvP.util.KitsManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class KitManagerCommand implements CommandExecutor {
	
	private IslandEarth plugin;
	
	public KitManagerCommand(IslandEarth plugin) {
		this.plugin = plugin;
	}

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {

    	if(sender instanceof Player) 
    	{
    		Player player = (Player) sender;
    		if(player.hasPermission("IslandEarth.kitmanager")) 
    		{
    			
    			switch(args.length) 
    			{
    				case 0:
    					sendHelp(player);
    					break;
    				case 1:
    					sendHelp(player);
    					break;
    				case 2:
						KitsManager km = new KitsManager(plugin);
						String kit = args[1];
    					switch(args[0].toLowerCase())
    					{
    						case "addkit":
    							km.addKit(player, kit);
    							break;
    						case "removekit":
    							km.removeKit(player, kit);
    							break;
    						case "givekit":
    							km.giveKit(player, kit);
    							break;
    						default:
    							sendHelp(player);
    							break;
    					}
    					
    					break;
    					
    				default:
    					sendHelp(player);
    					break;
    			}
    			
    		} else player.sendMessage(ChatColor.RED + "You don't have permission to execute this command!");
    	} else sender.sendMessage(ChatColor.RED + "You must be a player to run this command.");
        return true;
    }
    
    private void sendHelp(Player player) 
    {
    	TextComponent help = new TextComponent(ChatColor.YELLOW + "Showing help for KitsManager " + ChatColor.WHITE + "1/1");
		help.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Showing page 1/1 click to go to the next page or use /rr 2.").create()));
		player.spigot().sendMessage(help);
		
		TextComponent c1 = new TextComponent(ChatColor.GREEN + "/KitManager");
		c1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/KitManager"));
		player.spigot().sendMessage(c1);
		player.sendMessage(ChatColor.WHITE + "   Aliases: /km.");
		player.sendMessage(ChatColor.WHITE + "   Description: Displays help page.");
		
		TextComponent c2 = new TextComponent(ChatColor.GREEN + "/KitManager addkit [kit]");
		c2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/KitManager addkit "));
		player.spigot().sendMessage(c2);
		player.sendMessage(ChatColor.WHITE + "   Aliases: None.");
		player.sendMessage(ChatColor.WHITE + "   Description: Add a kit.");
		player.sendMessage(ChatColor.WHITE + "   Permission(s): OP");
		
		player.sendMessage(ChatColor.YELLOW + "© 2018 IslandEarth. Made with" + " ❤ " + "by SamB440 and EntityLlamaSpit.");
    }
}
