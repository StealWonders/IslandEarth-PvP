package net.IslandEarth.PvP;

import net.IslandEarth.PvP.command.KitManagerCommand;
import net.IslandEarth.PvP.listener.GameListener;
import net.IslandEarth.PvP.listener.GlobalListener;
import net.IslandEarth.PvP.listener.ServerPingListener;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

public class IslandEarth extends JavaPlugin {
	
	private String p = "[IslandEarth PvP]";
	
	@Override
	public void onEnable() {
		
		long s = System.nanoTime();
		info("Starting initialization of IslandEarth (PvP) v" + getDescription().getVersion());

        registerListeners();
        registerCommands();
        createFiles();

		long timing = (System.nanoTime() - s) / 10000000;
		info("Initialization complete! (" + timing + " ms)");
	}

	private void registerListeners() {

		getServer().getPluginManager().registerEvents(new ServerPingListener(), this);
		getServer().getPluginManager().registerEvents(new GameListener(this), this);
		getServer().getPluginManager().registerEvents(new GlobalListener(), this);
	}
	
	private void registerCommands() {
		
		getCommand("kitmanager").setExecutor(new KitManagerCommand(this));
	}
	
	private void createFiles() {
		File file = new File(getDataFolder() + "/kits");
		if(!file.exists()) file.mkdir();
	}

	private void registerManagers() { }
	
	private void info(String message) 
	{
		getLogger().info(p + " " + message);
	}

	private void error(String message) 
	{
		getLogger().warning(p + " " + message); 
	}
}
